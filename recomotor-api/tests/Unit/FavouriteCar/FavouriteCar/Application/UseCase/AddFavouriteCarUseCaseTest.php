<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\FavouriteCar\Application\UseCase;

use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\Car\Domain\Services\FindCarModelByIdService;
use App\FavouriteCar\FavouriteCar\Application\DTO\AddFavouriteCarUseCaseRequest;
use App\FavouriteCar\FavouriteCar\Application\Exceptions\CarModelNotExistsException;
use App\FavouriteCar\FavouriteCar\Application\Exceptions\UserNotExistsException;
use App\FavouriteCar\FavouriteCar\Application\UseCase\AddFavouriteCarUseCase;
use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Services\CreateFavouriteCarService;
use App\FavouriteCar\FavouriteCar\Domain\Services\SearchFavouriteCarService;
use App\FavouriteCar\FavouriteCar\Domain\Services\UpdateFavouriteCarService;
use App\FavouriteCar\User\Domain\Services\FindUserById;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AddFavouriteCarUseCaseTest extends TestCase
{
    private AddFavouriteCarUseCase $sut;
    private MockObject $searchFavouriteCarService;
    private MockObject $findUserById;
    private MockObject $findCarModelByIdService;
    private MockObject $createFavouriteCarService;
    private MockObject $updateFavouriteCarService;

    public function setUp(): void
    {
        $this->searchFavouriteCarService = $this->createMock(SearchFavouriteCarService::class);
        $this->findUserById = $this->createMock(FindUserById::class);
        $this->findCarModelByIdService = $this->createMock(FindCarModelByIdService::class);
        $this->createFavouriteCarService = $this->createMock(CreateFavouriteCarService::class);
        $this->updateFavouriteCarService = $this->createMock(UpdateFavouriteCarService::class);

        $this->sut = new AddFavouriteCarUseCase(
            $this->searchFavouriteCarService,
            $this->findUserById,
            $this->findCarModelByIdService,
            $this->createFavouriteCarService,
            $this->updateFavouriteCarService
        );
    }

    public function test_create_favourite_car_success(): void
    {
        $userId = 1;
        $carModelId = 2;

        $user = $this->createMock(User::class);
        $user->method('getId')->willReturn($userId);

        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn($carModelId);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $this->findUserById
            ->expects(self::once())
            ->method('find')
            ->with($userId)
            ->willReturn($user);

        $this->findCarModelByIdService
            ->expects(self::once())
            ->method('find')
            ->with($carModelId)
            ->willReturn($carModel);

        $this->searchFavouriteCarService
            ->expects(self::once())
            ->method('search')
            ->with($userId)
            ->willReturn(null);

        $this->createFavouriteCarService
            ->expects(self::once())
            ->method('create')
            ->with($carModelId, $userId);

        $this->updateFavouriteCarService
            ->expects(self::never())
            ->method('update');

        $this->sut->execute(
            new AddFavouriteCarUseCaseRequest(
                $userId,
                $carModelId
            )
        );
    }

    public function test_update_favourite_car_success(): void
    {
        $userId = 1;
        $carModelId = 2;

        $user = $this->createMock(User::class);
        $user->method('getId')->willReturn($userId);

        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn($carModelId);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $favouriteCar = $this->createMock(FavouriteCar::class);

        $this->findUserById
            ->expects(self::once())
            ->method('find')
            ->with($userId)
            ->willReturn($user);

        $this->findCarModelByIdService
            ->expects(self::once())
            ->method('find')
            ->with($carModelId)
            ->willReturn($carModel);

        $this->searchFavouriteCarService
            ->expects(self::once())
            ->method('search')
            ->with($userId)
            ->willReturn($favouriteCar);

        $this->createFavouriteCarService
            ->expects(self::never())
            ->method('create');

        $this->updateFavouriteCarService
            ->expects(self::once())
            ->method('update')
            ->with($favouriteCar, $carModelId);

        $this->sut->execute(
            new AddFavouriteCarUseCaseRequest(
                $userId,
                $carModelId
            )
        );
    }

    public function test_throw_error_car_model_id(): void
    {
        $this->expectException(CarModelNotExistsException::class);

        $userId = 1;
        $carModelId = 2;

        $user = $this->createMock(User::class);
        $user->method('getId')->willReturn($userId);

        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn($carModelId);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $this->findUserById
            ->expects(self::once())
            ->method('find')
            ->with($userId)
            ->willReturn($user);

        $this->findCarModelByIdService
            ->expects(self::once())
            ->method('find')
            ->with($carModelId)
            ->willReturn(null);

        $this->searchFavouriteCarService
            ->expects(self::never())
            ->method('search');

        $this->createFavouriteCarService
            ->expects(self::never())
            ->method('create');

        $this->updateFavouriteCarService
            ->expects(self::never())
            ->method('update');

        $this->sut->execute(
            new AddFavouriteCarUseCaseRequest(
                $userId,
                $carModelId
            )
        );
    }

    public function test_throw_error_user_id(): void
    {
        $this->expectException(UserNotExistsException::class);
        
        $userId = 1;
        $carModelId = 2;

        $this->findUserById
            ->expects(self::once())
            ->method('find')
            ->with(1)
            ->willReturn(null);

        $this->findCarModelByIdService
            ->expects(self::never())
            ->method('find');

        $this->searchFavouriteCarService
            ->expects(self::never())
            ->method('search');

        $this->createFavouriteCarService
            ->expects(self::never())
            ->method('create');

        $this->updateFavouriteCarService
            ->expects(self::never())
            ->method('update');

        $this->sut->execute(
            new AddFavouriteCarUseCaseRequest(
                $userId,
                $carModelId
            )
        );
    }
}
