<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;
use App\FavouriteCar\FavouriteCar\Domain\Services\UpdateFavouriteCarService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UpdateFavouriteCarServiceTest extends TestCase
{
    private UpdateFavouriteCarService $sut;
    private MockObject $favouriteCarRepository;

    public function setUp(): void
    {
        $this->favouriteCarRepository = $this->createMock(FavouriteCarRepository::class);

        $this->sut = new UpdateFavouriteCarService(
            $this->favouriteCarRepository
        );
    }

    public function test_update_favourite_car_success(): void
    {
        $userId = 1;
        $carModelId = 2;
        $carModelIdUpdated = 2;

        $favouriteCar = new FavouriteCar();
        $favouriteCar->setCarModelId($carModelId);
        $favouriteCar->setUserId($userId);

        $favouriteCarUpdated = clone $favouriteCar;
        $favouriteCarUpdated->setCarModelId($carModelIdUpdated);

        $this->favouriteCarRepository
            ->expects(self::once())
            ->method('save')
            ->with($favouriteCarUpdated)
            ->willReturn($favouriteCarUpdated);

        $result = $this->sut->update(
            $favouriteCar,
            $carModelId
        );

        $this->assertEquals($result, $favouriteCarUpdated);
    }
}
