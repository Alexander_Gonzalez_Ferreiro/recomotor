<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;
use App\FavouriteCar\FavouriteCar\Domain\Services\CreateFavouriteCarService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateFavouriteCarServiceTest extends TestCase
{
    private CreateFavouriteCarService $sut;
    private MockObject $favouriteCarRepository;

    public function setUp(): void
    {
        $this->favouriteCarRepository = $this->createMock(FavouriteCarRepository::class);

        $this->sut = new CreateFavouriteCarService(
            $this->favouriteCarRepository
        );
    }

    public function test_create_favourite_car_success(): void
    {
        $userId = 1;
        $carModelId = 2;

        $favouriteCar = new FavouriteCar();
        $favouriteCar->setCarModelId($carModelId);
        $favouriteCar->setUserId($userId);

        $this->favouriteCarRepository
            ->expects(self::once())
            ->method('save')
            ->with($favouriteCar)
            ->willReturn($favouriteCar);

        $result = $this->sut->create(
            $carModelId,
            $userId
        );

        $this->assertEquals($result, $favouriteCar);
    }
}
