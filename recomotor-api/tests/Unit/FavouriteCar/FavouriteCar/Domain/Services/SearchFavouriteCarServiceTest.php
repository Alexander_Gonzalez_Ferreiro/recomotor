<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;
use App\FavouriteCar\FavouriteCar\Domain\Services\SearchFavouriteCarService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SearchFavouriteCarServiceTest extends TestCase
{
    private SearchFavouriteCarService $sut;
    private MockObject $favouriteCarRepository;

    public function setUp(): void
    {
        $this->favouriteCarRepository = $this->createMock(FavouriteCarRepository::class);

        $this->sut = new SearchFavouriteCarService(
            $this->favouriteCarRepository
        );
    }

    public function test_update_favourite_car_success(): void
    {
        $userId = 1;
        $carModelId = 2;

        $favouriteCar = new FavouriteCar();
        $favouriteCar->setCarModelId($carModelId);
        $favouriteCar->setUserId($userId);
        
        $this->favouriteCarRepository
            ->expects(self::once())
            ->method('getFavouriteCarByUserId')
            ->with($userId)
            ->willReturn($favouriteCar);

        $result = $this->sut->search(
            $userId
        );

        $this->assertEquals($result, $favouriteCar);
    }
}
