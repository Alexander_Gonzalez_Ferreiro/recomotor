<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\User\Application\UseCase;

use App\FavouriteCar\User\Application\DTO\LoginUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserAlreadyExistsException;
use App\FavouriteCar\User\Application\Exceptions\UserNotExistsException;
use App\FavouriteCar\User\Application\UseCase\LoginUserUseCase;
use App\FavouriteCar\User\Domain\Services\FindUserByEmployeeNumber;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LoginUserUseCaseTest extends TestCase
{
    private LoginUserUseCase $sut;
    private MockObject $findUserByEmployeeNumber;

    public function setUp(): void
    {
        $this->findUserByEmployeeNumber = $this->createMock(FindUserByEmployeeNumber::class);

        $this->sut = new LoginUserUseCase(
            $this->findUserByEmployeeNumber
        );
    }

    public function test_login_user_success(): void
    {
        $employeeNumber = 'asdf';
        $name = 'Alejandro';

        $user = $this->createMock(User::class);
        $user->method('getName')->willReturn($name);
        $user->method('getEmployeeNumber')->willReturn($employeeNumber);

        $this->findUserByEmployeeNumber
            ->expects(self::once())
            ->method('find')
            ->with($employeeNumber)
            ->willReturn($user);

        $this->sut->execute(
            new LoginUserUseCaseRequest(
                $employeeNumber
            )
        );
    }

    public function test_throw_error_user_not_exists(): void
    {
        $this->expectException(UserNotExistsException::class);
        
        $employeeNumber = 'asdf';
        
        $this->findUserByEmployeeNumber
            ->expects(self::once())
            ->method('find')
            ->with($employeeNumber)
            ->willReturn(null);

        $this->sut->execute(
            new LoginUserUseCaseRequest(
                $employeeNumber
            )
        );
    }
}
