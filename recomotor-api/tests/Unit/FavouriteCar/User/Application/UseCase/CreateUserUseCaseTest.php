<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\User\Application\UseCase;

use App\FavouriteCar\User\Application\DTO\CreateUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserAlreadyExistsException;
use App\FavouriteCar\User\Application\UseCase\CreateUserUseCase;
use App\FavouriteCar\User\Domain\Services\CreateUserService;
use App\FavouriteCar\User\Domain\Services\FindUserByEmployeeNumber;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateUserUseCaseTest extends TestCase
{
    private CreateUserUseCase $sut;
    private MockObject $findUserByEmployeeNumber;
    private MockObject $createUserService;

    public function setUp(): void
    {
        $this->findUserByEmployeeNumber = $this->createMock(FindUserByEmployeeNumber::class);
        $this->createUserService = $this->createMock(CreateUserService::class);

        $this->sut = new CreateUserUseCase(
            $this->findUserByEmployeeNumber,
            $this->createUserService
        );
    }

    public function test_create_user_success(): void
    {
        $employeeNumber = 'asdf';
        $name = 'Alejandro';

        $user = $this->createMock(User::class);
        $user->method('getName')->willReturn($name);
        $user->method('getEmployeeNumber')->willReturn($employeeNumber);

        $this->findUserByEmployeeNumber
            ->expects(self::once())
            ->method('find')
            ->with($employeeNumber)
            ->willReturn(null);

        $this->sut->execute(
            new CreateUserUseCaseRequest(
                $employeeNumber,
                $name
            )
        );
    }

    public function test_throw_error_user_already_exists(): void
    {
        $this->expectException(UserAlreadyExistsException::class);
        
        $employeeNumber = 'asdf';
        $name = 'Alejandro';

        $user = $this->createMock(User::class);
        $user->method('getName')->willReturn($name);
        $user->method('getEmployeeNumber')->willReturn($employeeNumber);

        $this->findUserByEmployeeNumber
            ->expects(self::once())
            ->method('find')
            ->with($employeeNumber)
            ->willReturn($user);

        $this->sut->execute(
            new CreateUserUseCaseRequest(
                $employeeNumber,
                $name
            )
        );
    }
}
