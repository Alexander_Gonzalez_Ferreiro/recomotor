<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\Interfaces\UserRepository;
use App\FavouriteCar\User\Domain\Services\FindUserByEmployeeNumber;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FindUserByEmployeeNumberTest extends TestCase
{
    private FindUserByEmployeeNumber $sut;
    private MockObject $userRepository;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepository::class);

        $this->sut = new FindUserByEmployeeNumber(
            $this->userRepository
        );
    }

    public function test_find_user_success(): void
    {
        $employeeNumber = 'asdf';
        $name = 'Alejandro';

        $user = new User();
        $user->setName($name);
        $user->setEmployeeNumber($employeeNumber);

        $this->userRepository
            ->expects(self::once())
            ->method('getUserByEmployeeNumber')
            ->with($employeeNumber)
            ->willReturn($user);

        $result = $this->sut->find(
            $employeeNumber
        );

        $this->assertEquals($result, $user);
    }
}
