<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\Interfaces\UserRepository;
use App\FavouriteCar\User\Domain\Services\CreateUserService;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateUserServiceTest extends TestCase
{
    private CreateUserService $sut;
    private MockObject $userRepository;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepository::class);

        $this->sut = new CreateUserService(
            $this->userRepository
        );
    }

    public function test_create_user_success(): void
    {
        $employeeNumber = 'asdf';
        $name = 'Alejandro';

        $user = new User();
        $user->setName($name);
        $user->setEmployeeNumber($employeeNumber);

        $this->userRepository
            ->expects(self::once())
            ->method('save')
            ->with($user)
            ->willReturn($user);

        $this->sut->create(
            $employeeNumber,
            $name
        );
    }
}
