<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\Interfaces\UserRepository;
use App\FavouriteCar\User\Domain\Services\FindUserById;
use App\FavouriteCar\User\Domain\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FindUserByIdTest extends TestCase
{
    private FindUserById $sut;
    private MockObject $userRepository;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepository::class);

        $this->sut = new FindUserById(
            $this->userRepository
        );
    }

    public function test_find_user_by_id_success(): void
    {
        $userId = 2;
        $employeeNumber = 'abcd';
        $name = 'Alejandro';

        $user = new User();
        $user->setName($name);
        $user->setEmployeeNumber($employeeNumber);

        $this->userRepository
            ->expects(self::once())
            ->method('getUserById')
            ->with($userId)
            ->willReturn($user);

        $result = $this->sut->find(
            $userId
        );

        $this->assertEquals($result, $user);
    }
}
