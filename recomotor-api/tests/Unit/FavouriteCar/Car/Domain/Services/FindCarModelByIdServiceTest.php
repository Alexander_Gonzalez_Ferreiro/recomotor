<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\Car\Domain\Services;

use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\Car\Domain\Interfaces\CarModelRepository;
use App\FavouriteCar\Car\Domain\Services\FindCarModelByIdService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FindCarModelByIdServiceTest extends TestCase
{
    private FindCarModelByIdService $sut;
    private MockObject $carModelRepository;

    public function setUp(): void
    {
        $this->carModelRepository = $this->createMock(CarModelRepository::class);
        $this->sut = new FindCarModelByIdService(
            $this->carModelRepository
        );
    }

    public function test_find_car_model(): void
    {
        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn(1);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $this->carModelRepository
            ->expects(self::once())
            ->method('getCarModelById')
            ->with(1)
            ->willReturn($carModel);

        $result = $this->sut->find(1);
        $this->assertEquals($result, $carModel);
    }
}
