<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\Car\Domain\Converters;

use App\FavouriteCar\Car\Domain\Converters\CarModelConverter;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelDataObject;
use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;
use PHPUnit\Framework\TestCase;

class CarModelConverterTest extends TestCase
{
    private CarModelConverter $sut;

    public function setUp(): void
    {
        $this->sut = new CarModelConverter();
    }

    public function test_return_car_model_converted(): void
    {
        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn(1);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $carDataObject = new CarModelDataObject();
        $carDataObject->id = 1;
        $carDataObject->name = 'Arosa';
        $carDataObject->image_link = 'ImageLink';
        $carDataObject->brandName = 'Seat';

        $result = $this->sut->execute($carModel);
        $this->assertEquals($result, $carDataObject);
    }
}
