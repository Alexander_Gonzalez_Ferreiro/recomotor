<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\Car\Domain\Converters;

use App\FavouriteCar\Car\Domain\Converters\CarModelConverter;
use App\FavouriteCar\Car\Domain\Converters\CarModelsListConverter;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelDataObject;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelListDataObject;
use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CarModelsListConverterTest extends TestCase
{
    private CarModelsListConverter $sut;
    private MockObject $carModelConverter;

    public function setUp(): void
    {
        $this->carModelConverter = $this->createMock(CarModelConverter::class);
        $this->sut = new CarModelsListConverter(
            $this->carModelConverter
        );
    }

    public function test_return_car_model_list_converted(): void
    {
        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn(1);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $carDataObject = new CarModelDataObject();
        $carDataObject->id = 1;
        $carDataObject->name = 'Arosa';
        $carDataObject->image_link = 'ImageLink';
        $carDataObject->brandName = 'Seat';

        $carListDataObject = new CarModelListDataObject();
        $carListDataObject->carModelList = [$carDataObject];


        $this->carModelConverter
            ->expects(self::once())
            ->method('execute')
            ->with($carModel)
            ->willReturn($carDataObject);

        $result = $this->sut->execute([$carModel]);
        $this->assertEquals($result, $carListDataObject);
    }
}
