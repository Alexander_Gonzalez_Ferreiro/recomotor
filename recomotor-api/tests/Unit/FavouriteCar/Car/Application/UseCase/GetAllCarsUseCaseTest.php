<?php
declare(strict_types = 1);

namespace Tests\Unit\FavouriteCar\Car\Application\UseCase;

use App\FavouriteCar\Car\Application\UseCase\GetAllCarsUseCase;
use App\FavouriteCar\Car\Domain\Converters\CarModelsListConverter;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelDataObject;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelListDataObject;
use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\Car\Domain\Services\SearchCarsService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetAllCarsUseCaseTest extends TestCase
{
    private GetAllCarsUseCase $sut;
    private MockObject $searchCarsService;
    private MockObject $carModelsListConverter;

    public function setUp(): void
    {
        $this->searchCarsService = $this->createMock(SearchCarsService::class);
        $this->carModelsListConverter = $this->createMock(CarModelsListConverter::class);

        $this->sut = new GetAllCarsUseCase(
            $this->searchCarsService,
            $this->carModelsListConverter
        );
    }

    public function test_return_car_models(): void
    {
        $carBrand = $this->createMock(CarBrand::class);
        $carBrand->method('getName')->willReturn('Seat');

        $carModel = $this->createMock(CarModel::class);
        $carModel->method('getId')->willReturn(1);
        $carModel->method('getName')->willReturn('Arosa');
        $carModel->method('getImageLink')->willReturn('ImageLink');
        $carModel->method('getBrand')->willReturn($carBrand);

        $carDataObject = new CarModelDataObject();
        $carDataObject->id = 1;
        $carDataObject->name = 'Arosa';
        $carDataObject->image_link = 'ImageLink';
        $carDataObject->brandName = 'Seat';

        $carListDataObject = new CarModelListDataObject();
        $carListDataObject->carModelList = [$carDataObject];

        $this->searchCarsService
            ->expects(self::once())
            ->method('search')
            ->willReturn([$carModel]);

        $this->carModelsListConverter
            ->expects(self::once())
            ->method('execute')
            ->with([$carModel])
            ->willReturn($carListDataObject);

        $result = $this->sut->execute();
        $this->assertEquals($result, $carListDataObject);
    }
}
