<?php

use App\FavouriteCar\Car\Infraestructure\GetAllCarsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cars', 'App\FavouriteCar\Car\Infraestructure\GetAllCarsController@index');
Route::post('/user', 'App\FavouriteCar\User\Infraestructure\CreateUserController@index');
Route::post('/login-user', 'App\FavouriteCar\User\Infraestructure\LoginUserController@index');
Route::post('/favourite', 'App\FavouriteCar\FavouriteCar\Infraestructure\AddFavouriteCarController@index');
