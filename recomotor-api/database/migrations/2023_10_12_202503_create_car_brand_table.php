<?php

use App\Shared\Domain\Enum\CarBrandIdsEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('car_brand', function (Blueprint $table) {
            $table->id('car_brand_id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        DB::table('car_brand')->insert(
            array(
                [
                    'car_brand_id' => CarBrandIdsEnum::Seat,
                    'name' => 'Seat'
                ],
                [
                    'car_brand_id' => CarBrandIdsEnum::MercedesBenz,
                    'name' => 'Mercedes-Benz'
                ],
                [
                    'car_brand_id' => CarBrandIdsEnum::Peugeot,
                    'name' => 'Peugeot'
                ],
                [
                    'car_brand_id' => CarBrandIdsEnum::Nissan,
                    'name' => 'Nissan'
                ],
                [
                    'car_brand_id' => CarBrandIdsEnum::Audi,
                    'name' => 'Audi'
                ]
            ),
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('car_brand');
    }
};
