<?php

use App\Shared\Domain\Enum\CarBrandIdsEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('car_model', function (Blueprint $table) {
            $table->id('car_model_id');
            $table->string('name')->unique();
            $table->text('image_link');
            $table->unsignedBigInteger('car_brand_id');
            $table->timestamps();
 
            $table->foreign('car_brand_id')->references('car_brand_id')->on('car_brand');
        });

        DB::table('car_model')->insert(
            array(
                [
                    'name' => 'Arona',
                    'image_link' => 'https://www.diariomotor.com/imagenes/2018/03/skoda_lanzamiento_su_seat_arona_00.jpg',
                    'car_brand_id' => CarBrandIdsEnum::Seat
                ],
                [
                    'name' => 'Ibiza',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::Seat
                ],
                [
                    'name' => 'Leon',
                    'image_link' => 'https://www.topgear.com/sites/default/files/images/news-article/carousel/2019/01/4e9fb55f7ef03a11029c2fb4864fb621/20181201_siemoneitracing_vader_leoncuprast_001_front34_online.jpg',
                    'car_brand_id' => CarBrandIdsEnum::Seat
                ],
                [
                    'name' => 'Classe A',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::MercedesBenz
                ],
                [
                    'name' => 'Classe C',
                    'image_link' => 'https://www.autobild.es/sites/autobild.es/public/dc/fotos/batch_20C0673_002.jpg',
                    'car_brand_id' => CarBrandIdsEnum::MercedesBenz
                ],
                [
                    'name' => 'Classe G',
                    'image_link' => 'https://img.remediosdigitales.com/9afd76/21c0550_001/1366_2000.jpeg',
                    'car_brand_id' => CarBrandIdsEnum::MercedesBenz
                ],
                [
                    'name' => '306',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::Peugeot
                ],
                [
                    'name' => '406',
                    'image_link' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQckrOZz_I1oeV3efjMpuTZqhfhbz09nEcGmYqREiXN&s',
                    'car_brand_id' => CarBrandIdsEnum::Peugeot
                ],
                [
                    'name' => 'Partner',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::Peugeot
                ],
                [
                    'name' => 'Almera',
                    'image_link' => 'https://www.autoscout24.es/cms-content-assets/2g0k6EgGWyAzDAjcU2xZIl-8cdbf2a75557b35a85ed332d204620c6-nissan-almera-l-01-1100.jpg',
                    'car_brand_id' => CarBrandIdsEnum::Nissan
                ],
                [
                    'name' => 'Juke',
                    'image_link' => 'https://cdn.motor1.com/images/mgl/rbrEE/s1/2018-nissan-juke-euro-spec.jpg',
                    'car_brand_id' => CarBrandIdsEnum::Nissan
                ],
                [
                    'name' => 'Qashqai',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::Nissan
                ],
                [
                    'name' => 'A4',
                    'image_link' => 'https://images.prismic.io/carwow/d87f95a9-0970-45b1-ac67-86642f39dce7_Audi-A4_08-1.jpg?fit=clip&q=60&w=750&cs=tinysrgb&auto=format',
                    'car_brand_id' => CarBrandIdsEnum::Audi
                ],
                [
                    'name' => 'RS3',
                    'image_link' => 'https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Audi_A3_2020.jpg?itok=MkiCOhpe',
                    'car_brand_id' => CarBrandIdsEnum::Audi
                ],
                [
                    'name' => 'Q7',
                    'image_link' => '',
                    'car_brand_id' => CarBrandIdsEnum::Audi
                ]
            ),
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('car_model');
    }
};
