<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Infraestructure;

use App\FavouriteCar\User\Application\DTO\CreateUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserAlreadyExistsException;
use App\FavouriteCar\User\Application\UseCase\CreateUserUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;

class CreateUserController extends Controller
{    
    public function __construct(
        private CreateUserUseCase $createUserUseCase,
        private Request $request
    ) {
    }

    public function index(): JsonResponse
    {
        $request = new CreateUserUseCaseRequest(
            $this->request::post('employeeNumber'),
            $this->request::post('name')
        );
        try {
            return response()->json(
                $this->createUserUseCase->execute($request)
            );
        } catch (UserAlreadyExistsException $th) {
            return response()->json(
                [
                    'message' => $th->getMessage(),
                    'code' => $th->getCode()
                ],
                $th->getCode()
            );
        }
    }
}
