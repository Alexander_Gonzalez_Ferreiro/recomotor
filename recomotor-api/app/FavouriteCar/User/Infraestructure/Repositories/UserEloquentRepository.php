<?php
declare(strict_types = 1);

namespace App\FavouriteCar\User\Infraestructure\Repositories;

use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Interfaces\UserRepository;

class UserEloquentRepository implements UserRepository
{
    public function getUserById(int $userId): ?User
    {
        return User::where('user_id', '=', $userId)->first();
    }

    public function getUserByEmployeeNumber(string $employeeNumber): ?User
    {
        return User::where('employee_number', '=', $employeeNumber)->first();
    }

    public function save(User $user): User
    {
        $user->save();
        return $user;
    }
}