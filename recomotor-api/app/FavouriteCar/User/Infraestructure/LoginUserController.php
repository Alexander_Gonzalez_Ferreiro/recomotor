<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Infraestructure;

use App\FavouriteCar\User\Application\DTO\LoginUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserNotExistsException;
use App\FavouriteCar\User\Application\UseCase\LoginUserUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;

class LoginUserController extends Controller
{    
    public function __construct(
        private LoginUserUseCase $loginUserUseCase,
        private Request $request
    ) {
    }

    public function index(): JsonResponse
    {
        $request = new LoginUserUseCaseRequest(
            $this->request::post('employeeNumber')
        );
        try {
            return response()->json(
                $this->loginUserUseCase->execute($request)
            );
        } catch (UserNotExistsException $th) {
            return response()->json(
                [
                    'message' => $th->getMessage(),
                    'code' => $th->getCode()
                ],
                $th->getCode()
            );
        }
    }
}
