<?php
declare(strict_types = 1);

namespace App\FavouriteCar\User\Application\Exceptions;

use Exception;
use Illuminate\Http\Response;

class UserAlreadyExistsException extends Exception
{
    public function __construct()
    {
        $this->message = 'Users already exists';
        $this->code = Response::HTTP_BAD_REQUEST;
    }
}
