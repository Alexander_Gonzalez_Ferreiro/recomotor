<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Application\DTO;
 
class CreateUserUseCaseRequest
{    
    public function __construct(
        private string $employeeNumber,
        private string $name
    ) {
    }

    public function getEmployeeNumber(): string
    {
        return $this->employeeNumber;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
