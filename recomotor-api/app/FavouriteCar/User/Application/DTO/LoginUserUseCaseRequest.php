<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Application\DTO;
 
class LoginUserUseCaseRequest
{    
    public function __construct(
        private string $employeeNumber
    ) {
    }

    public function getEmployeeNumber(): string
    {
        return $this->employeeNumber;
    }
}
