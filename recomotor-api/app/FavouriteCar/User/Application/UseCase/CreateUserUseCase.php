<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Application\UseCase;

use App\FavouriteCar\User\Application\DTO\CreateUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserAlreadyExistsException;
use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Services\CreateUserService;
use App\FavouriteCar\User\Domain\Services\FindUserByEmployeeNumber;

class CreateUserUseCase
{    
    public function __construct(
        private FindUserByEmployeeNumber $findUserByEmployeeNumber,
        private CreateUserService $createUserService
    ) {
    }

    public function execute(CreateUserUseCaseRequest $request): array
    {
        $user = $this->findUserByEmployeeNumber->find($request->getEmployeeNumber());

        $this->userExist($user);

        $user = $this->createUserService->create(
            $request->getEmployeeNumber(),
            $request->getName()
        );

        return ['id' => $user->getId(), 'favouriteCarModel' => $user->getFavouriteCar()?->getCarModelId()];
    }

    private function userExist(?User $user): void
    {
        if ($user != null) {
            throw new UserAlreadyExistsException();
        }
    }
}
