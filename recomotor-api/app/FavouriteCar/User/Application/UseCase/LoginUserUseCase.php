<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Application\UseCase;

use App\FavouriteCar\User\Application\DTO\LoginUserUseCaseRequest;
use App\FavouriteCar\User\Application\Exceptions\UserNotExistsException;
use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Services\FindUserByEmployeeNumber;

class LoginUserUseCase
{    
    public function __construct(
        private FindUserByEmployeeNumber $findUserByEmployeeNumber
    ) {
    }

    public function execute(LoginUserUseCaseRequest $request): array
    {
        $user = $this->findUserByEmployeeNumber->find($request->getEmployeeNumber());

        $this->userExist($user);

        return ['id' => $user->getId(), 'favouriteCarModel' => $user->getFavouriteCar()?->getCarModelId()];
    }

    private function userExist(?User $user): void
    {
        if ($user == null) {
            throw new UserNotExistsException();
        }
    }
}
