<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Interfaces\UserRepository;

class FindUserByEmployeeNumber
{
    public function __construct(
        private UserRepository $userRepository
    ) {    
    }

    public function find(string $employeeNumber): ?User
    {
        return $this->userRepository->getUserByEmployeeNumber($employeeNumber);
    }
}
