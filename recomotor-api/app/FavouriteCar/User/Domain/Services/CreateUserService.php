<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Interfaces\UserRepository;

class CreateUserService
{
    public function __construct(
        private UserRepository $userRepository
    ) {    
    }

    public function create(string $employeeNumber, string $name): User
    {
        $user = new User();
        $user->setName($name);
        $user->setEmployeeNumber($employeeNumber);

        return $this->userRepository->save($user);
    }
}
