<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Domain\Services;

use App\FavouriteCar\User\Domain\User;
use App\FavouriteCar\User\Domain\Interfaces\UserRepository;

class FindUserById
{
    public function __construct(
        private UserRepository $userRepository
    ) {    
    }

    public function find(int $userId): ?User
    {
        return $this->userRepository->getUserById($userId);
    }
}
