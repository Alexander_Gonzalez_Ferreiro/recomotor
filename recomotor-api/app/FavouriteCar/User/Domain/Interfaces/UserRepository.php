<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Domain\Interfaces;

use App\FavouriteCar\User\Domain\User;

interface UserRepository
{
    public function getUserById(int $userId): ?User;
    public function getUserByEmployeeNumber(string $employeeNumber): ?User;
    public function save(User $user): User;
}
