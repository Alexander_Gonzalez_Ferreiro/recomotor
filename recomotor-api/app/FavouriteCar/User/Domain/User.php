<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\User\Domain;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class User extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';

    public function getId(): int
    {
        return $this->getAttribute('user_id');
    }

    public function getName(): string
    {
        return $this->getAttribute('name');
    }

    public function getEmployeeNumber(): string
    {
        return $this->getAttribute('employee_number');
    }

    public function setName(string $name): void
    {
        $this->setAttribute('name', $name);
    }

    public function setEmployeeNumber(string $employeeNumber): void
    {
        $this->setAttribute('employee_number', $employeeNumber);
    }

    public function favouriteCar(): ?HasOne
    {
        return $this->hasOne(FavouriteCar::class, 'user_id', 'user_id');
    }

    public function getFavouriteCar(): ?FavouriteCar
    {
        return $this->favouriteCar;
    }

}