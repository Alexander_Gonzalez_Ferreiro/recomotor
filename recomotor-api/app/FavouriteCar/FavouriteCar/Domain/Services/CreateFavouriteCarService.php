<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;

class CreateFavouriteCarService
{
    public function __construct(
        private FavouriteCarRepository $favouriteCarRepository
    ) {    
    }

    public function create(int $carModelId, int $userId): ?FavouriteCar
    {
        $favouriteCar = new FavouriteCar();
        $favouriteCar->setCarModelId($carModelId);
        $favouriteCar->setUserId($userId);

        return $this->favouriteCarRepository->save($favouriteCar);
    }
}
