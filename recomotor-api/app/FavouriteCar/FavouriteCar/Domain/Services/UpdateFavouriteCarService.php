<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;

class UpdateFavouriteCarService
{
    public function __construct(
        private FavouriteCarRepository $favouriteCarRepository
    ) {    
    }

    public function update(FavouriteCar $favouriteCar, int $carModelId): ?FavouriteCar
    {
        $favouriteCar->setCarModelId($carModelId);

        return $this->favouriteCarRepository->save($favouriteCar);
    }
}
