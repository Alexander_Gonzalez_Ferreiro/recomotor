<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Domain\Services;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;

class SearchFavouriteCarService
{
    public function __construct(
        private FavouriteCarRepository $favouriteCarRepository
    ) {    
    }

    public function search(int $userId): ?FavouriteCar
    {
        return $this->favouriteCarRepository->getFavouriteCarByUserId($userId);
    }
}
