<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Domain;

use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\User\Domain\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class FavouriteCar extends Model
{
    protected $table = 'favourite_car';
    protected $primaryKey = 'favourite_car_id';

    public function getId(): int
    {
        return $this->getAttribute('favourite_car_id');
    }

    public function getUserId(): int
    {
        return $this->getAttribute('user_id');
    }

    public function setUserId(int $userId): void
    {
        $this->setAttribute('user_id', $userId);
    }

    public function getCarModelId(): int
    {
        return $this->getAttribute('car_model_id');
    }

    public function setCarModelId(int $carModelId): void
    {
        $this->setAttribute('car_model_id', $carModelId);
    }

    private function user(): HasOne
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }

    public function getUser(): CarModel
    {
        return $this->getRelation('user');
    }

    private function carModel(): HasOne
    {
        return $this->hasOne(CarModel::class, 'car_model_id', 'car_model_id');
    }

    public function getCarModel(): CarModel
    {
        return $this->getRelation('carModel');
    }
}