<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Domain\Interfaces;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;

interface FavouriteCarRepository
{
    public function getFavouriteCarByUserId(int $userId): ?FavouriteCar;
    public function save(FavouriteCar $favouriteCar): ?FavouriteCar;
}
