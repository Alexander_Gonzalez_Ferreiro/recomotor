<?php
declare(strict_types = 1);

namespace App\FavouriteCar\FavouriteCar\Application\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarModelNotExistsException extends Exception
{
    public function __construct()
    {
        $this->message = 'Car model not exists';
        $this->code = Response::HTTP_NOT_FOUND;
    }
}
