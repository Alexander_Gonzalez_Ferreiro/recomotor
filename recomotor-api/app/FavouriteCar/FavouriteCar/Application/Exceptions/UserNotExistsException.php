<?php
declare(strict_types = 1);

namespace App\FavouriteCar\FavouriteCar\Application\Exceptions;

use Exception;
use Illuminate\Http\Response;

class UserNotExistsException extends Exception
{
    public function __construct()
    {
        $this->message = 'Users not exists';
        $this->code = Response::HTTP_NOT_FOUND;
    }
}
