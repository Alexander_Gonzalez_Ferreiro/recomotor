<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Application\DTO;
 
class AddFavouriteCarUseCaseRequest
{    
    public function __construct(
        private int $userId,
        private int $carModelId
    ) {
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getModelId(): int
    {
        return $this->carModelId;
    }
}
