<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Application\UseCase;

use App\FavouriteCar\Car\Domain\Services\FindCarModelByIdService;
use App\FavouriteCar\FavouriteCar\Application\DTO\AddFavouriteCarUseCaseRequest;
use App\FavouriteCar\FavouriteCar\Application\Exceptions\CarModelNotExistsException;
use App\FavouriteCar\FavouriteCar\Application\Exceptions\UserNotExistsException;
use App\FavouriteCar\FavouriteCar\Domain\Services\CreateFavouriteCarService;
use App\FavouriteCar\FavouriteCar\Domain\Services\SearchFavouriteCarService;
use App\FavouriteCar\FavouriteCar\Domain\Services\UpdateFavouriteCarService;
use App\FavouriteCar\User\Domain\Services\FindUserById;
 
class AddFavouriteCarUseCase
{    
    public function __construct(
        private SearchFavouriteCarService $searchFavouriteCarService,
        private FindUserById $findUserById,
        private FindCarModelByIdService $findCarModelByIdService,
        private CreateFavouriteCarService $createFavouriteCarService,
        private UpdateFavouriteCarService $updateFavouriteCarService
    ) {
    }

    public function execute(AddFavouriteCarUseCaseRequest $request): void
    {
        $this->userExists($request->getUserId());
        $this->carModelExists($request->getModelId());
        $favouriteCar = $this->searchFavouriteCarService->search($request->getUserId());

        if ($favouriteCar == null) {
            $this->createFavouriteCarService->create($request->getModelId(), $request->getUserId());
            return;
        }
        
        $this->updateFavouriteCarService->update($favouriteCar, $request->getModelId());
    }

    private function userExists(int $userId): void
    {
        if (
            $this->findUserById->find($userId) == null
        ) {
            throw new UserNotExistsException();
        }
    }

    private function carModelExists(int $carModel): void
    {
        if (
            $this->findCarModelByIdService->find($carModel) == null
        ) {
            throw new CarModelNotExistsException();
        }
    }
}
