<?php
declare(strict_types = 1);

namespace App\FavouriteCar\FavouriteCar\Infraestructure\Repositories;

use App\FavouriteCar\FavouriteCar\Domain\FavouriteCar;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;

class FavouriteCarEloquentRepository implements FavouriteCarRepository
{
    public function getFavouriteCarByUserId(int $userId): ?FavouriteCar
    {
        return FavouriteCar::where('user_id', '=', $userId)->first();
    }

    public function save(FavouriteCar $favouriteCar): ?FavouriteCar
    {
        $favouriteCar->save();

        return $favouriteCar;
    }
}