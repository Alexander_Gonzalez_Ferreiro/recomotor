<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\FavouriteCar\Infraestructure;

use App\FavouriteCar\FavouriteCar\Application\UseCase\AddFavouriteCarUseCase;
use App\FavouriteCar\FavouriteCar\Application\DTO\AddFavouriteCarUseCaseRequest;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;

class AddFavouriteCarController extends Controller
{    
    public function __construct(
        private AddFavouriteCarUseCase $addFavouriteCarUseCase,
        private Request $request
    ) {
    }

    public function index(): JsonResponse
    {
        $request = new AddFavouriteCarUseCaseRequest(
            (int) $this->request::post('userId'),
            (int) $this->request::post('carModelId')
        );
        try {
            return response()->json(
                $this->addFavouriteCarUseCase->execute($request)
            );
        } catch (Exception $th) {
            return response()->json(
                $th->getMessage(),
                $th->getCode()
            );
        }
    }
}
