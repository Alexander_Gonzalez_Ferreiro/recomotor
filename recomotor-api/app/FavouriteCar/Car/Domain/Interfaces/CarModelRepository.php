<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain\Interfaces;

use App\FavouriteCar\Car\Domain\Entities\CarModel;

interface CarModelRepository
{
    public function getAllCarModels(): array;
    public function getCarModelById(int $carModelId): ?CarModel;
}
