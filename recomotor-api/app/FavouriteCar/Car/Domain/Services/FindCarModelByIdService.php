<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain\Services;

use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\Car\Domain\Interfaces\CarModelRepository;
 
class FindCarModelByIdService
{
    public function __construct(
        private CarModelRepository $carModelRepository
    ) {    
    }

    public function find(int $carModelId): ?CarModel
    {
        return $this->carModelRepository->getCarModelById($carModelId);
    }
}
