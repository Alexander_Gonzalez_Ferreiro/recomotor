<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain\Services;

use App\FavouriteCar\Car\Domain\Interfaces\CarModelRepository;
 
class SearchCarsService
{
    public function __construct(
        private CarModelRepository $carModelRepository
    ) {    
    }

    public function search(): array
    {
        return $this->carModelRepository->getAllCarModels();
    }
}
