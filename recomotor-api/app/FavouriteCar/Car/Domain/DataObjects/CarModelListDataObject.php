<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Domain\DataObjects;

class CarModelListDataObject
{
    public array $carModelList;
}
