<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Domain\DataObjects;

class CarModelDataObject
{
    public int $id;
    public string $name;
    public string $image_link;
    public string $brandName;
}
