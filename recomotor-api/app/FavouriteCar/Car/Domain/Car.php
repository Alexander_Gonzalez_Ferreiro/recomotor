<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain;

use App\FavouriteCar\Car\Domain\Entities\CarBrand;
use App\FavouriteCar\Car\Domain\Entities\CarModel;

class Car
{
    public function __construct(
        private CarBrand $brand,
        private CarModel $model
    )
    {}

    public function getBrandObj(): CarBrand
    {
        return $this->brand;
    }

    public function getModelObj(): CarModel
    {
        return $this->model;
    }
}