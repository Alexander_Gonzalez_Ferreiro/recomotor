<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Domain\Converters;

use App\FavouriteCar\Car\Domain\DataObjects\CarModelDataObject;
use App\FavouriteCar\Car\Domain\Entities\CarModel;

class CarModelConverter
{
    public function execute(CarModel $carModel): CarModelDataObject
    {
        $dataObject = new CarModelDataObject();

        $dataObject->id = $carModel->getId();
        $dataObject->name = $carModel->getName();
        $dataObject->image_link = $carModel->getImageLink();
        $dataObject->brandName = $carModel->getBrand()->getName();

        return $dataObject;
    }
}