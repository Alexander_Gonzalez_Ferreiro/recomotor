<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Domain\Converters;

use App\FavouriteCar\Car\Domain\DataObjects\CarModelListDataObject;

class CarModelsListConverter
{
    public function __construct(
        private CarModelConverter $carModelConverter
    ) { 
    }

    public function execute(array $carModels): CarModelListDataObject
    {
        $carModelsList = new CarModelListDataObject();

        foreach ($carModels as $model) {
            $carModelsList->carModelList[] = $this->carModelConverter->execute($model);
        }

        return $carModelsList;
    }
}