<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain\Entities;
 
use Illuminate\Database\Eloquent\Model;
 
class CarBrand extends Model
{
    protected $table = 'car_brand';
    protected $primaryKey = 'car_brand_id';

    public function getId(): int
    {
        return $this->getAttribute('car_brand_id');
    }

    public function getName(): string
    {
        return $this->getAttribute('name');
    }
}