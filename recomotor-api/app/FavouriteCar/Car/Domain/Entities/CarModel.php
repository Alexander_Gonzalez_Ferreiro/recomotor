<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Domain\Entities;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CarModel extends Model
{
    protected $table = 'car_model';
    protected $primaryKey = 'car_model_id';

    public function getId(): int
    {
        return $this->getAttribute('car_model_id');
    }

    public function getName(): string
    {
        return $this->getAttribute('name');
    }

    public function getImageLink(): string
    {
        return $this->getAttribute('image_link');
    }

    public function brand(): HasOne
    {
        return $this->hasOne(CarBrand::class, 'car_brand_id', 'car_brand_id');
    }

    public function getBrand(): CarBrand
    {
        return $this->brand;
    }
}