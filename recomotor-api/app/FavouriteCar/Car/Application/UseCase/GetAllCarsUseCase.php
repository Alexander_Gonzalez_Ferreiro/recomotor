<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Application\UseCase;

use App\FavouriteCar\Car\Domain\Converters\CarModelsListConverter;
use App\FavouriteCar\Car\Domain\DataObjects\CarModelListDataObject;
use App\FavouriteCar\Car\Domain\Services\SearchCarsService;
 
class GetAllCarsUseCase
{
    public function __construct(
        private SearchCarsService $searchCarsService,
        private CarModelsListConverter $carModelsListConverter
    ) {    
    }

    public function execute(): CarModelListDataObject
    {
        $carModels = $this->searchCarsService->search();
        return $this->carModelsListConverter->execute($carModels);
    }
}
