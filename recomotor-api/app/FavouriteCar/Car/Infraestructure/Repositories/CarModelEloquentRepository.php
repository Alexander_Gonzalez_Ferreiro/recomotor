<?php
declare(strict_types = 1);

namespace App\FavouriteCar\Car\Infraestructure\Repositories;

use App\FavouriteCar\Car\Domain\Entities\CarModel;
use App\FavouriteCar\Car\Domain\Interfaces\CarModelRepository;

class CarModelEloquentRepository implements CarModelRepository
{
    public function getAllCarModels(): array
    {
        $carsArray = [];
        $cars = CarModel::with(['brand'])->get();

        foreach ($cars as $car) {
            $carsArray[] = $car;
        }

        return $carsArray;
    }

    public function getCarModelById(int $carModelId): ?CarModel
    {
        return CarModel::where('car_model_id', '=', $carModelId)->first();
    }
}
