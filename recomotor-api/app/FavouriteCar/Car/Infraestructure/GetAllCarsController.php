<?php
declare(strict_types = 1);
 
namespace App\FavouriteCar\Car\Infraestructure;

use App\FavouriteCar\Car\Application\UseCase\GetAllCarsUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
 
class GetAllCarsController extends Controller
{    
    public function __construct(
        private GetAllCarsUseCase $getAllCarsUseCase
    ) {
    }

    public function index(): JsonResponse
    {
        return response()->json(
           $this->getAllCarsUseCase->execute()
        );
    }
}
