<?php
declare(strict_types = 1);

namespace App\Shared\Domain\Enum;

enum CarBrandIdsEnum: int
{
    case Seat = 1;
    case MercedesBenz = 2;
    case Peugeot = 3;
    case Nissan = 4;
    case Audi = 5;
}
