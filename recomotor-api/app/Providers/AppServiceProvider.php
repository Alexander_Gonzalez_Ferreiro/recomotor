<?php

namespace App\Providers;

use App\FavouriteCar\Car\Domain\Interfaces\CarModelRepository;
use App\FavouriteCar\Car\Infraestructure\Repositories\CarModelEloquentRepository;
use App\FavouriteCar\FavouriteCar\Infraestructure\Repositories\FavouriteCarEloquentRepository;
use App\FavouriteCar\FavouriteCar\Domain\Interfaces\FavouriteCarRepository;
use App\FavouriteCar\User\Domain\Interfaces\UserRepository;
use App\FavouriteCar\User\Infraestructure\Repositories\UserEloquentRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(CarModelRepository::class, CarModelEloquentRepository::class);
        $this->app->bind(UserRepository::class, UserEloquentRepository::class);
        $this->app->bind(FavouriteCarRepository::class, FavouriteCarEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
