# recomotor

## Installation
- docker-compose up
- cd .\recomotor-api\
- composer install
- php artisan migrate
- php arisan serve
- cd ..
- cd .\recomotor-admin\
- npm install
- npm run serve

Open admin -> http://localhost:8080

Connect to api -> http://127.0.0.1:8000/api

## Project status
Se ha acabado con la base de datos y el backend de la aplicación. Se ha creado un pequeño admin para gestionar las llamadas a base de datos.
Se ha generado los tests unitarios del backend que se ejecutan con "php artisan test".
Lo ultimo que ha faltado ha sido la dockerizacion del proyecto para poder levantar un entorno de desarrollo sin necesidad de levantar los servicios
por separado.

## Url to connect

GET->Get Cars: http://127.0.0.1:8000/api/cars

POST->Register: http://127.0.0.1:8000/api/user
{
    "employeeNumber": string,
    "name": string
}

POST->Login: http://127.0.0.1:8000/api/login-user
{
    "employeeNumber": string
}

POST->Add to favourite: http://127.0.0.1:8000/api/favourite
{
    "userId": int,
    "carModelId": int
}