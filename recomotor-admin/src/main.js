import 'bootstrap/dist/css/bootstrap.css'
import VueRouter from 'vue-router'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'
import { createApp } from 'vue'
import App from './App.vue'

createApp(App).use(router).use(VueRouter).use(VueSweetalert2).mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
import router from './router'