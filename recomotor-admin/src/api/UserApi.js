export class UserApi {
    USER_URL = 'http://127.0.0.1:8000/api/';
    constructor() { 
    }
    
    static createUser(employeeNumber, employeeName) {
            new Promise((resolve, reject) => {
                fetch(
                    this.USER_URL + 'user',
                    {
                        'employeeNumber': employeeNumber,
                        'name': employeeName
                    }
                    ).then((data) => {
                        resolve(data);
                    }).catch((error) => {
                        reject(error)
                    })
            })
        }
        
        static loginUser(employeeNumber) {
            fetch(
                this.USER_URL + 'login-user',
                {
                    'employeeNumber': employeeNumber
                }
                ).then((data) => {
                    return data;
                }).catch((error) => {
                    this.$swal({
                        icon: 'error',
                        text: error.message
                    })
                })
            }
        }