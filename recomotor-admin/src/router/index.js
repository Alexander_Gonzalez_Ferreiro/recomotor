import { createRouter, createWebHistory } from 'vue-router'
import LoginAndRegistryView from '../views/LoginAndRegistryView.vue'
import CarsView from '../views/CarsView.vue'

const routes = [
  {
    path: '/',
    name: 'login',
    component: LoginAndRegistryView
  },
  {
    path: '/cars',
    name: 'cars',
    component: CarsView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !localStorage.user) next({ name: 'login' })
  else next()
})

export default router
